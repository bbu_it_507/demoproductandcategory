package com.example.demoproductandcategory.constan;

import com.example.demoproductandcategory.models.request.ItemKeyValue;
import com.example.demoproductandcategory.models.request.StockType;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static List<ItemKeyValue> getAllStatus(){
        List<ItemKeyValue> itemKeyValues = new ArrayList<>();
        itemKeyValues.add(new ItemKeyValue(1,"ACT","Active"));
        itemKeyValues.add(new ItemKeyValue(2,"DEL","Delete"));
        itemKeyValues.add(new ItemKeyValue(3,"DSL","Disable"));
        return itemKeyValues;
    }
    public static List<String> getAllStatusString(){
        List<String> statustList = new ArrayList<>();
        statustList.add("ACT");
        statustList.add("DEL");
        statustList.add("DSL");
        return statustList;
    }
    public static List<StockType> getAllStatusListType(){
        List<StockType> stockType = new ArrayList<>();
        stockType.add(new StockType(1,"CTS","Cute Stock"));
        stockType.add(new StockType(2,"NST","Not Stock"));
        stockType.add(new StockType(3,"CMS","Coming Soon "));
        return stockType;
    }



}
